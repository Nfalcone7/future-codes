#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
from pylab import *
from scipy.fftpack import fft
from scipy.interpolate import interp1d

##################################################
##     strain                                   ##
##################################################
def strain(t, x, y, cutoff):
   # Interpolate to uniform grid:
   tU     = np.linspace(t[0],max(t),len(t))
   jR     = interp1d(t,x)
   jI     = interp1d(t,y)
   xU     = jR(tU)
   yU     = jI(tU)
   zU     = xU+1j*yU
   # Compute the FFT:
   fftU   = fft(zU)
   tstep  = tU[1]-tU[0]
   N      = zU.size
   # Compute the frequencies of the FFT:
   fftU_f = fftfreq(N,d=tstep)
   # Kill the DC offset:
   fftU[0]= 0.0
   # Change the low-end frequencies
   freqs=fftU_f
   # Make sure zero frequency does not cause error
   freqs[0]=1.0
   for i in range(len(fftU_f)):
      if (abs(fftU_f[i]) < cutoff):
         freqs[i] = cutoff*sign(fftU_f[i])
      else:
         freqs[i] = fftU_f[i]
   # Do the integration and inverse FFT:
   strain = -ifft(fftU/((2.0*pi*freqs)**2))
   return tU, strain
##################################################

#
#  USER can set these:
#

# Number of points to use:
numt   = 100
# Frequency
omega = 1.5
# Amplitude
amp = 2.0
# Initial value for x:
initialx = amp

#
#  This is the code:
#

dx     = (10.0-0.0)/(numt+1)
# Set up some grid:
myt    = np.linspace(0.0,10.0,numt)
# Create an array x(t) defined at these same points (and initialize to 0):
myx    = 0.0*myt
# Now integrate numerically assuming (dx/dt) = amp*sin( omega*t)
myx[0] = initialx
for i in range(1,numt):
   dxdt_i   = amp*sin( omega*myt[i] )
   dxdt_im1 = amp*sin(omega*myt[i-1])
   dxdt     = 0.5*(dxdt_i + dxdt_im1)
   myx[i]   = myx[i-1]+dx*(dxdt)
   #myx[i] = myx[i-1]+dx*0.5*(3.0*myt[i-1]**3+3.0*myt[i]**3)
# Also get analytic solution:
anax = amp*cos(omega*myt)/omega
#anax = (3.0/4.0)*myt**4+initialx

pl.plot(myt,myx)
pl.plot(myt,anax)
pl.tight_layout(pad=0.1)
plt.subplots_adjust(wspace=.5, hspace=0.2)
#pl.tight_layout()

pl.draw()

pl.savefig("output.pdf")
#pl.show()

