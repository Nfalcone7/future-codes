import numpy as np
import matplotlib.pyplot as pl
from pylab import *
from scipy.fftpack import fft
from scipy.interpolate import interp1d

def strain(t, x, y, cutoff):
    tU    = np.linspace(t[0],max(t),len(t))
    jR    = interp1d(t,x)
    jI    = interp1d(t,y)
    xU    = jR(tU)
    yU    = jI(tU)
    zU    = xU+1j*yU
    fftU  = fft(zU)
    tstep = tU[1]-tU[0]
    N     = zU.size
    fftU_f = fftfreq(N,d=tstep)
    fftU[0]=1.0
    for i in range(len(fftU_f)):
        if (abs(fftU_f[i]) < cutoff):
            freqs[i] = cutoff*sign(fftU_f[i])
        else:
            freqs[i] = fftU_f[i]
    strain = -ifft(fftU/((2.0*pi*freqs)**2))
    return tU, strain
numt   = 100
initialx = 5.0
dx     = (10.0-0.0)/(numt+1)
myt    = np.linspace(0.0,10.0,numt)
myx    = 0.0*myt
myx[0] = initialx
for i in range(1,numt):
    myx[i] = myx[0]+dx*0.5*(3.0*myt[i-1]**3+3.0*myt[i]**3)
anax = (3.0/4.0)*myt**4+initialx
pl.plot(myt,myx)
pl.plot(myt,anax)
pl.tight_layout(pad=0.1)
plt.subplots_adjust(wspace=.5, hspace=0.2)
pl.draw()
pl.savefig("output.pdf")
